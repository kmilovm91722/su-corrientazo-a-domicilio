package app;

import model.DroneManager;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Stream;

/**
 * Entry point of the application
 */
public class Application {
    public static ResourceBundle translations;
    public static PropertiesConfiguration properties;

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            System.out.println("Usage: java -jar su-corrientazo-a-domicilio.jar <INPUT_FILE_PATH_1> <INPUT_FILE_PATH_2>... <INPUT_FILE_PATH_N>");
            return;
        }
        init();
        int numberOfDrones = Application.properties.getInt("numberOfDrones");
        if (args.length > numberOfDrones) {
            System.out.println(Application.translations.getString("maxDronesError") + " (" + numberOfDrones + ").");
        }
        DroneManager droneManager = new DroneManager();
        Stream.of(args).forEach(inputFilePath -> droneManager.sendDrone(inputFilePath));
    }

    private static void initProperties() {
        try {
            Application.properties = new Configurations().properties(new File("application.properties"));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }

    }

    private static void initTranslations() {
        Application.translations = ResourceBundle.getBundle("words", new Locale(
                Application.properties.getString("localeLanguage"),
                Application.properties.getString("localeCountry")));
    }

    public static void init() {
        initProperties();
        initTranslations();
    }
}
