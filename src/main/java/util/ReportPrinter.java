package util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Helper class used to print drone status reports to files.
 * It keeps track of the number of reports printed to have the output files accordingly named.
 */
public class ReportPrinter {

    private int reportNumber;
    private String prefix;
    private String suffix;

    public ReportPrinter(String prefix, String suffix) {
        this.prefix = prefix;
        this.suffix = suffix;
        this.reportNumber = 1;
    }

    /**
     * Prints the given report to a file in the application execution path and names the file using the
     * current report number, the prefix and suffix specified in the application.properties.
     * @param report
     */
    public void printReport(String report) {
        String filePath = System.getProperty("user.dir").concat("/").concat(this.prefix)
                .concat(String.valueOf(this.reportNumber)).concat(suffix);
        try {
            Files.write(Paths.get(filePath), report.getBytes());
            reportNumber++;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
