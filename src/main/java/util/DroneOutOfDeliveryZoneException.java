package util;

import app.Application;

/**
 *  This exception is thrown when a drone position is outside the specific delivery zone size in the application.properties
 */
public class DroneOutOfDeliveryZoneException extends RuntimeException {

    public DroneOutOfDeliveryZoneException() {
        super(Application.translations.getString("droneOutOfDeliveryZoneError"));
    }
}
