package util;

/**
 * This exception is thrown when the amount of lunches per drone exceeds the value specified in the application.properties
 */
public class DroneMaxCapacityException extends RuntimeException {

    public DroneMaxCapacityException(String message) {
        super(message);
    }
}
