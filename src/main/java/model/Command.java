package model;

import java.util.Arrays;

/**
 * An enum to apply commands over drones by its abbreviation
 */
public enum Command {

    MOVE_FORWARD("A") {
        @Override
        public void apply(Drone drone) {
            drone.moveForward();
        }
    },
    TURN_RIGHT("D") {
        @Override
        public void apply(Drone drone) {
            drone.turnRight();
        }
    },
    TURN_LEFT("I") {
        @Override
        public void apply(Drone drone) {
            drone.turnLeft();
        }
    },
    ;

    private String value;

    private Command(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public abstract void apply(Drone drone);

    public static Command getInstance(String value) {
        return Arrays.stream(values()).filter(command -> value.equals(command.getValue())).findFirst().orElse(null);
    }
}
