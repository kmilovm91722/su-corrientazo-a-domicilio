package model;

import app.Application;
import util.DroneMaxCapacityException;
import util.ReportPrinter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Manager to be in charge of sending drones. The responsibility of managing concurrency restrictions in the future
 * should be implemented here.
 */
public class DroneManager {

    private ReportPrinter reportPrinter;

    public DroneManager() {
        this.reportPrinter = new ReportPrinter(
                Application.properties.getString("reportPrefix"), Application.properties.getString("reportSuffix"));
    }

    /**
     * Reads the given deliveryCommandsFilePath and sends a drone to deliver a lunch for each line of commands.
     * @param deliveryCommandsFilePath
     */
    public void sendDrone(String deliveryCommandsFilePath) {
        int allowedDeliveriesPerTrip = Application.properties.getInt("allowedDeliveriesPerTrip");
        try {
            if (Files.lines(Paths.get(deliveryCommandsFilePath)).count() > allowedDeliveriesPerTrip) {
                throw new DroneMaxCapacityException(Application.translations.getString("maxCapacityError") + "(" +
                        allowedDeliveriesPerTrip + ")");
            }
            Drone drone = new Drone();
            StringBuilder report = new StringBuilder(Application.translations.getString("deliveryReport"));
            Files.lines(Paths.get(deliveryCommandsFilePath)).forEach(line -> {
                drone.deliverLunch(line);
                report.append("\n\n").append(drone.getCurrentStatusReport());
            });
            this.reportPrinter.printReport(report.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
