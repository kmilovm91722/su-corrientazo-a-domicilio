package model;

import app.Application;
import util.DroneOutOfDeliveryZoneException;

/**
 * This class represents a Drone and it manages its movement logic and commands execution
 */
public class Drone {

    private static final Orientation[] ORIENTATIONS = new Orientation[] {Orientation.NORTH, Orientation.EAST, Orientation.SOUTH, Orientation.WEST};
    /**
     * The drone orientation is represented by the position in the ORIENTATIONS array.
     */
    private int orientationIndex;
    private Position position;
    private static final int DELIVERY_ZONE_SIZE_X = Application.properties.getInt("deliveryZoneSizeX");
    private static final int DELIVERY_ZONE_SIZE_Y = Application.properties.getInt("deliveryZoneSizeY");

    /**
     * Basic constructor, creates a drone in the position (0,0) pointing to North
     */
    public Drone() {
        this.orientationIndex = 0;
        this.position = new Position(0, 0);
    }

    /**
     * Validates that the drone position cannot go above or below the limits defined in the configuration for
     * the delivery zone.
     */
    private void validatePosition() {
        if (this.position.getX() < DELIVERY_ZONE_SIZE_X * -1 ||
                this.position.getX() > DELIVERY_ZONE_SIZE_X ||
                this.position.getY() < DELIVERY_ZONE_SIZE_Y * -1 ||
                this.position.getY() > DELIVERY_ZONE_SIZE_Y) {
            throw new DroneOutOfDeliveryZoneException();
        }
    }

    /**
     * Moves the drone forward in the direction which the drone is pointing to. It also calls validatePosition
     * to be sure that after every movement the drone is still in the valid delivery zone.
     */
    public void moveForward() {
        this.position = this.getOrientation().moveForward(this.position);
        validatePosition();
    }

    /**
     * Changes the orientationIndex to point in the next orientation according to the ORIENTATIONS array
     */
    public void turnRight() {
        orientationIndex = orientationIndex < ORIENTATIONS.length - 1 ? orientationIndex + 1 : 0;
    }

    /**
     * Changes the orientationIndex to point in the previous orientation according to the ORIENTATIONS array
     */
    public void turnLeft() {
        orientationIndex = orientationIndex > 0 ? orientationIndex - 1 : ORIENTATIONS.length - 1;
    }

    /**
     * @return The drone current orientation
     */
    public Orientation getOrientation() {
        return ORIENTATIONS[this.orientationIndex];
    }

    /**
     * Parses the commands parameter string and executes every command.
     * @param commands
     */
    public void deliverLunch(String commands) {
        if (commands == null || commands.length() == 0) {
            return;
        }
        for (int i = 0; i < commands.length(); i++) {
            Command.getInstance(String.valueOf(commands.charAt(i))).apply(this);
        }
    }

    public Position getPosition() {
        return position;
    }

    /**
     * Enum that has the encapsulation to move in the right way according to each orientation.
     */
    public enum Orientation {
        NORTH("north") {
            @Override
            public Position moveForward(Position position) {
                return new Position(position.getX(), position.getY() + 1);
            }
        },
        EAST("east") {
            @Override
            public Position moveForward(Position position) {
                return new Position(position.getX() + 1, position.getY());
            }
        },
        SOUTH("south") {
            @Override
            public Position moveForward(Position position) {
                return new Position(position.getX(), position.getY() - 1);
            }
        },
        WEST("west") {
            @Override
            public Position moveForward(Position position) {
                return new Position(position.getX() - 1, position.getY());
            }
        };

        private String value;

        private Orientation(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public abstract Position moveForward(Position position);
    }

    /**
     * @return A string with the current position of the drone and its orientation.
     */
    public String getCurrentStatusReport() {
        StringBuilder sb = new StringBuilder("(")
                .append(this.position.getX()).append(", ")
                .append(this.position.getY())
                .append(") ")
                .append(Application.translations.getString("orientation")).append(" ")
                .append(Application.translations.getString(this.getOrientation().getValue()));

        return sb.toString();
    }
}
