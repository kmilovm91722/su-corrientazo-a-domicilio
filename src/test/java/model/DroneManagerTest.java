package model;

import app.Application;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import util.DroneMaxCapacityException;

import java.io.File;

public class DroneManagerTest {

    @BeforeAll
    public static void setup() {
        Application.init();
        PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration();
        propertiesConfiguration.setProperty("allowedDeliveriesPerTrip", 3);
        Application.properties = propertiesConfiguration;
    }

    @Test
    public void droneManagerDoesNotSendMoreThanTheAllowedNumberOfLunchesPerDrone() {
        DroneManager droneManager = new DroneManager();
        DroneMaxCapacityException e = Assertions.assertThrows(DroneMaxCapacityException.class, () -> {
            droneManager.sendDrone(getResourceAbsolutePath("deliveryCapacityCase.txt"));
        });
    }

    private String getResourceAbsolutePath(String resourceFileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(resourceFileName).getFile());
        return file.getAbsolutePath();
    }
}
