package model;

import app.Application;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import util.DroneOutOfDeliveryZoneException;

public class DroneTest {

    @BeforeAll
    public static void setup() {
        Application.init();
        PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration();
        propertiesConfiguration.setProperty("deliveryZoneSizeX", 10);
        propertiesConfiguration.setProperty("deliveryZoneSizeY", 10);
        Application.properties = propertiesConfiguration;
    }

    @Test
    public void droneDeliversLunchAndFinishesAtTheCorrectLocation() {
        Drone drone = new Drone();
        drone.deliverLunch("AAAAIAA");
        Assertions.assertEquals(-2, drone.getPosition().getX());
        Assertions.assertEquals(4, drone.getPosition().getY());
        Assertions.assertEquals(Drone.Orientation.WEST, drone.getOrientation());

        /**
         * This is one of the cases that were wrong in the coding test specification
         * the expected output according the specification document was
         * (-3, 3) - South. As you can see; the correct result is (-1, 3) - South
         */
        drone.deliverLunch("DDDAIAD");
        Assertions.assertEquals(-1, drone.getPosition().getX());
        Assertions.assertEquals(3, drone.getPosition().getY());
        Assertions.assertEquals(Drone.Orientation.SOUTH, drone.getOrientation());

        /**
         * This case was also wrong in the coding test specification.
         * The correct result is (0, 0) - West instead of (-4, 2) - East
         */
        drone.deliverLunch("AAIADAD");
        Assertions.assertEquals(0, drone.getPosition().getX());
        Assertions.assertEquals(0, drone.getPosition().getY());
        Assertions.assertEquals(Drone.Orientation.WEST, drone.getOrientation());
    }

    @Test
    public void droneMovesNorth() {
        Drone drone = new Drone();
        drone.moveForward();
        Assertions.assertEquals(0, drone.getPosition().getX());
        Assertions.assertEquals(1, drone.getPosition().getY());
    }

    @Test
    public void droneMovesEast() {
        Drone drone = new Drone();
        drone.turnRight();
        drone.moveForward();
        Assertions.assertEquals(1, drone.getPosition().getX());
        Assertions.assertEquals(0, drone.getPosition().getY());
    }

    @Test
    public void droneMovesWest() {
        Drone drone = new Drone();
        drone.turnLeft();
        drone.moveForward();
        Assertions.assertEquals(-1, drone.getPosition().getX());
        Assertions.assertEquals(0, drone.getPosition().getY());
    }

    @Test
    public void droneMovesSouth() {
        Drone drone = new Drone();
        drone.turnLeft();
        drone.turnLeft();
        drone.moveForward();
        Assertions.assertEquals(0, drone.getPosition().getX());
        Assertions.assertEquals(-1, drone.getPosition().getY());
    }

    @Test
    public void droneCannotGoOutsideDeliveryZoneNorth() {
        Drone drone = new Drone();
        DroneOutOfDeliveryZoneException e = Assertions.assertThrows(DroneOutOfDeliveryZoneException.class, () -> {
            for (int i = 0; i < 11 ; i++) {
                drone.moveForward();
            }
        });
    }

    @Test
    public void droneCannotGoOutsideDeliveryZoneEast() {
        Drone drone = new Drone();
        drone.turnRight();
        DroneOutOfDeliveryZoneException e = Assertions.assertThrows(DroneOutOfDeliveryZoneException.class, () -> {
            for (int i = 0; i < 11 ; i++) {
                drone.moveForward();
            }
        });
    }

    @Test
    public void droneCannotGoOutsideDeliveryZoneWest() {
        Drone drone = new Drone();
        drone.turnLeft();
        DroneOutOfDeliveryZoneException e = Assertions.assertThrows(DroneOutOfDeliveryZoneException.class, () -> {
            for (int i = 0; i < 11 ; i++) {
                drone.moveForward();
            }
        });
    }

    @Test
    public void droneCannotGoOutsideDeliveryZoneSouth() {
        Drone drone = new Drone();
        drone.turnLeft();
        drone.turnLeft();
        DroneOutOfDeliveryZoneException e = Assertions.assertThrows(DroneOutOfDeliveryZoneException.class, () -> {
            for (int i = 0; i < 11 ; i++) {
                drone.moveForward();
            }
        });
    }
}
