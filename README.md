# Su corrientazo a domicilio (Drone Delivery Manager)

This is a console based Drone Manager Application. It reads a set of input files containing the commands to be executed
by each drone per trip.

The following rules apply:

- Each line represents a lunch delivery trip.
- Drones always start at (0,0) North.
- Each character in a line represents a command. Please see the available commands:
    
    - A: Move Forward
    - R: Turn Right
    - I: Turn Left
    
An output file will be created for each input file.    

## Usage

- Download the code from this repository

    `$ git clone https://kmilovm91722@bitbucket.org/kmilovm91722/su-corrientazo-a-domicilio.git`
    
- Build using Maven

    `$ cd su-corrientazo-a-domicilio/`
    
    `$ mvn clean package`

- Execute it
    
    `$ java -jar target/su-corrientazo-a-domicilio.jar <INPUT_FILE_PATH_1> <INPUT_FILE_PATH_2>... <INPUT_FILE_PATH_N>`
    
    Output files will be created in the same execution directory.
        